--BASIC QUERY

--a. create table sale_order
CREATE TABLE sale_order (
	partner_id  integer NOT NULL,
	Amount  numeric ( 50 ) NOT NULL
	 
);

--b. create table sale_order_batch
CREATE TABLE sale_order_batch  (
	partner_id  integer NOT NULL,
	Amount  numeric ( 50 ) NOT NULL
	 
);

--c. Insert dummy data into sale_order 
INSERT INTO public.sale_order(
	partner_id, amount)
	VALUES  (19, 1000)
			,(30, 2000)
			,(193, 1500)
			,(19, 4300)
			,(30, 2000);
			
--d. Create query to insert data from sale_order into sale_order_batch with grouping based on partner_id			
INSERT INTO public.sale_order_batch
SELECT partner_id,  sum(amount) as amount 
FROM public.sale_order
GROUP BY partner_id


--e. The records in sale_order_batch
select * from sale_order_batch

--f. create table pos_order
CREATE TABLE pos_order (
	Product_id  integer NOT NULL,
	Product_type  varchar ( 50 ) NOT NULL,
	Categ_id  integer NOT NULL,
	Amount numeric NOT NULL
	 
);


--g. create table pos_order_storable
CREATE TABLE pos_order_storable (
	Product_id  integer NOT NULL,
	Categ_id  integer NOT NULL,
	Amount numeric NOT NULL
	 
);


--h. create table pos_order_consumable
CREATE TABLE pos_order_consumable (
	Product_id  integer NOT NULL,
	Categ_id  integer NOT NULL,
	Amount numeric NOT NULL
	 
);

--i. create table pos_order_service
CREATE TABLE pos_order_service (
	Product_id  integer NOT NULL,
	Categ_id  integer NOT NULL,
	Amount numeric NOT NULL
	 
);


--j. Insert dummy data in pos_order table
INSERT INTO public.pos_order(
	Product_id, Categ_id, Product_type, Amount)
	VALUES  (19, 2,'consu',1000)
			,(30, 1,'storable',2000)
			,(193, 34,'service',1500)
			,(20, 12,'consu',4300)
			,(15, 3,'storable',2000);


--k. Create query to insert data from pos_order into pos_order_storable with grouping based on product_id and the product_type is storable
INSERT INTO public.pos_order_storable
SELECT product_id
,categ_id
,sum(amount) as amount 
FROM pos_order
WHERE product_type = 'storable'
GROUP BY product_id, categ_id


--l. The records in pos_order_storable
select * from pos_order_storable


--m. Create query to insert data from pos_order into pos_order_consumable with grouping based on product_id and the product_type is consu
INSERT INTO public.pos_order_consumable
SELECT product_id
,categ_id
,sum(amount) as amount 
FROM pos_order
WHERE product_type = 'consu'
GROUP BY product_id, categ_id

--n. The records in pos_order_consumable 
select * from pos_order_consumable


--o. Create query to insert data from pos_order into pos_order_service with grouping based on product_id and the product_type is service
INSERT INTO public.pos_order_service
SELECT product_id
,categ_id
,sum(amount) as amount 
FROM pos_order
WHERE product_type = 'service'
GROUP BY product_id, categ_id


--p. The records in pos_order_service
select * from pos_order_service
CREATE TABLE purchase_order (
	Partner_id  integer NOT NULL,
	Partner_name  varchar(50) NULL,
	Amount numeric NOT NULL
	 
);


--q. Create table purchase_order
CREATE TABLE purchase_order (
	Partner_id  integer NOT NULL,
	Partner_name  varchar(50) NULL,
	Amount numeric NOT NULL
	 
);


--r. Create table res_partner
CREATE TABLE res_partner (
	id  integer NOT NULL,
	name  varchar(50) NULL
	 
);

--s. Insert dummy data purchase_order
INSERT INTO public.purchase_order(
	partner_id, partner_name, amount)
	VALUES  (193, NULL,20000)
			,(19, NULL,29402)
			,(30, NULL,19000)
			,(93, NULL,12300);

--t. Insert dummy data res_partner
INSERT INTO public.res_partner(
	ID, name)
	VALUES  (19, 'Mat Midi')
			,(93, 'Rahvana')
			,(30, 'Sang Raksasa')
			,(193, 'Cak Chiku');
			

--u. Create query to update partner_name in purchase_order table from res_partner table
UPDATE purchase_order
SET partner_name = b.name
FROM res_partner b 
WHERE partner_id = b.id

--v.View result update in poin u
select * from purchase_order




