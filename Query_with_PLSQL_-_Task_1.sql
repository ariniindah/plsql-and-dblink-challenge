--Query with PL/SQL

--1. Create function to select from pos_order_table
create or replace function get_pos_order (
  id int, ad int
	
) 
	returns table (
		
		fProduct_type  varchar,
		fCateg_id  int,
		fAmount numeric 
	) 
	language plpgsql
as $$
begin
	return query 
		select
			Product_type,
			Categ_id,
			Amount
			
		from
			pos_order
		where Product_id = id or Product_id = ad;
end;$$

SELECT * from get_pos_order(193,19)


--2. Create procedure to insert res_partner with this parameter value
create procedure insert_res_partner(
   id int,
   name varchar 

)
language plpgsql    
as $$
begin
    -- insert into res_partner 
   insert into res_partner(
	ID, name)
	VALUES  (id, name);
			
		

    commit;
end;$$

call insert_res_partner(11,'Arini');

select * from res_partner

--3. Explore variable in function/procedure, create example case
CREATE OR REPLACE FUNCTION   somefuncname() 
RETURNS int 
LANGUAGE plpgsql AS $$
DECLARE
  one int;
  two int;
BEGIN
  one := 5;
  two := 5;
  RETURN one + two;
END
$$;
SELECT somefuncname();


do $$ 
declare
   counter    integer := 1;
   first_name varchar(50) := 'John';
   last_name  varchar(50) := 'Doe';
   payment    numeric(11,2) := 20.5;
begin 
   raise notice '% % % has been paid % USD', 
       counter, 
	   first_name, 
	   last_name, 
	   payment;
end $$;

----------------------------------------------------
CREATE OR REPLACE FUNCTION totalAmount ()
RETURNS integer AS $total$
declare
	total integer;
BEGIN
   SELECT count(*) into total FROM COMPANY;
   RETURN total;
END;
$total$ LANGUAGE plpgsql;


--4. Explore looping in function/procedure, create example case
-- Table: public.phonebook

-- DROP TABLE IF EXISTS public.phonebook;

CREATE TABLE IF NOT EXISTS public.phonebook
(
    nik character varying(32) COLLATE pg_catalog."default",
    firstname character varying(32) COLLATE pg_catalog."default",
    address character varying(64) COLLATE pg_catalog."default"
)

TABLESPACE pg_default;

ALTER TABLE IF EXISTS public.phonebook
    OWNER to postgres;
	
	
--- create proc
CREATE OR REPLACE PROCEDURE insert_data(v_nik int, v_firstname varchar(100), v_address varchar(100))
LANGUAGE SQL
AS $$
INSERT INTO public.phonebook(
  nik, firstname, address)
  VALUES (v_nik, v_firstname, v_address);
$$;


--looping

do $$
declare 
   maxloop integer:= 3;
begin
   for counter in 1..maxloop loop
  CALL insert_data(123, 'Annisa', 'jkt');
   end loop;
end; $$

select * from phonebook


--5.Explore if - else in function/procedure, create example case

DO
$do$
BEGIN
   IF EXISTS (SELECT FROM phonebook) THEN
      DELETE FROM phonebook;
	  --WHERE nik = '123';
   ELSE
      CALL insert_data(123, 'Annisa', 'jkt');
   END IF;
END
$do$

select * from public.phonebook